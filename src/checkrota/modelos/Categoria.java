package checkrota.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.GenericGenerator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import checkrota.bancodedados.HibernateUtil;

@Entity
@Table( name = "categorias" )
public class Categoria {
	private Long id;
	private String descricao;
	
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/*
	 * converte array de json em objetos e os salva
	 */
	public static void saveFromJson(JsonArray jsonArray) {
		if(jsonArray == null)
			return;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Gson gson = new Gson();
		jsonArray.forEach(json -> session.save(gson.fromJson(json, Categoria.class)));
		tx.commit();
		session.close();
	}
}
