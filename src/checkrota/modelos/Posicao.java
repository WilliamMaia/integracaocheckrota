package checkrota.modelos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.GenericGenerator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import checkrota.bancodedados.HibernateUtil;

@Entity
@Table( name = "posicoes" )
public class Posicao {
	
	private String carregando;
	private String categoriaVeiculoId;
	private String classificacaoVeiculoId;
	private String dataSistema;
	private String dataTransmissao;
	private String descricaoVeiculo;
	private Integer direcao;
	private Double distancia;
	private String dscMotivoTransmissao;
	private String dtTransmissao;
	private String fabricanteEquipamento;
	private String fornecedorChip;
	private String horimetro;
	private Double horimetroHoras;
	private Double horimetroValor;
	private String identificador;
	private Boolean ignicao;
	private Integer intensidadeGprs;
	private Double kmOdometro;
	private Double latitude;
	private Double longitude;
	private String modeloEquipamento;
	private Integer motivoTransmissaoId;
	private Float nivelCombustivel;
	private String nomeCliente;
	private Integer numEntrada2;
	private Integer numEntrada3;
	private Integer numEntrada4;
	private String numeroTel;
	private Double odometro;
	private String panico;
	private String placa;
	private String pontoDescricao;
	private Integer pontoId;
	private Integer qtdeSatelite;
	private Integer RPM;
	private String serialChip;
	private Boolean staEntrada1;
	private Boolean staEntrada2;
	private Boolean staEntrada3;
	private Boolean staEntrada4;
	private Boolean staEntrada5;
	private Boolean staEntrada6;
	private Boolean staEntrada7;
	private Boolean staEntrada8;
	private Boolean staSaida1;
	private Boolean staSaida2;
	private Boolean staSaida3;
	private Boolean staSaida4;
	private Boolean staSaida5;
	private Boolean staSaida6;
	private Boolean staSaida7;
	private Boolean staSaida8;
	private Double temperatura;
	private Double tensao;
	private Integer totalMensagemAberta;
	private Integer totalOcorrenciaAberta;
	private Integer totalRotaAtiva;
	private Integer veiculoId;
	private Double velocidade;
	
	private Long id;

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public Long getId() {
	    return id;
	}
	
	public Posicao() {
		super();
	}

	
	public void setId(Long id) {
		this.id = id;
	}

	public String getCarregando() {
		return carregando;
	}

	public void setCarregando(String carregando) {
		this.carregando = carregando;
	}

	public String getCategoriaVeiculoId() {
		return categoriaVeiculoId;
	}

	public void setCategoriaVeiculoId(String categoriaVeiculoId) {
		this.categoriaVeiculoId = categoriaVeiculoId;
	}

	public String getClassificacaoVeiculoId() {
		return classificacaoVeiculoId;
	}

	public void setClassificacaoVeiculoId(String classificacaoVeiculoId) {
		this.classificacaoVeiculoId = classificacaoVeiculoId;
	}

	public String getDataSistema() {
		return dataSistema;
	}

	public void setDataSistema(String dataSistema) {
		this.dataSistema = dataSistema;
	}

	public String getDataTransmissao() {
		return dataTransmissao;
	}

	public void setDataTransmissao(String dataTransmissao) {
		this.dataTransmissao = dataTransmissao;
	}

	public String getDescricaoVeiculo() {
		return descricaoVeiculo;
	}

	public void setDescricaoVeiculo(String descricaoVeiculo) {
		this.descricaoVeiculo = descricaoVeiculo;
	}

	public Integer getDirecao() {
		return direcao;
	}

	public void setDirecao(Integer direcao) {
		this.direcao = direcao;
	}

	public Double getDistancia() {
		return distancia;
	}

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}

	public String getDscMotivoTransmissao() {
		return dscMotivoTransmissao;
	}

	public void setDscMotivoTransmissao(String dscMotivoTransmissao) {
		this.dscMotivoTransmissao = dscMotivoTransmissao;
	}

	public String getDtTransmissao() {
		return dtTransmissao;
	}

	public void setDtTransmissao(String dtTransmissao) {
		this.dtTransmissao = dtTransmissao;
	}

	public String getFabricanteEquipamento() {
		return fabricanteEquipamento;
	}

	public void setFabricanteEquipamento(String fabricanteEquipamento) {
		this.fabricanteEquipamento = fabricanteEquipamento;
	}

	public String getFornecedorChip() {
		return fornecedorChip;
	}

	public void setFornecedorChip(String fornecedorChip) {
		this.fornecedorChip = fornecedorChip;
	}

	public String getHorimetro() {
		return horimetro;
	}

	public void setHorimetro(String horimetro) {
		this.horimetro = horimetro;
	}

	public Double getHorimetroHoras() {
		return horimetroHoras;
	}

	public void setHorimetroHoras(Double horimetroHoras) {
		this.horimetroHoras = horimetroHoras;
	}

	public Double getHorimetroValor() {
		return horimetroValor;
	}

	public void setHorimetroValor(Double horimetroValor) {
		this.horimetroValor = horimetroValor;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public Boolean getIgnicao() {
		return ignicao;
	}

	public void setIgnicao(Boolean ignicao) {
		this.ignicao = ignicao;
	}

	public Integer getIntensidadeGprs() {
		return intensidadeGprs;
	}

	public void setIntensidadeGprs(Integer intensidadeGprs) {
		this.intensidadeGprs = intensidadeGprs;
	}

	public Double getKmOdometro() {
		return kmOdometro;
	}

	public void setKmOdometro(Double kmOdometro) {
		this.kmOdometro = kmOdometro;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getModeloEquipamento() {
		return modeloEquipamento;
	}

	public void setModeloEquipamento(String modeloEquipamento) {
		this.modeloEquipamento = modeloEquipamento;
	}

	public Integer getMotivoTransmissaoId() {
		return motivoTransmissaoId;
	}

	public void setMotivoTransmissaoId(Integer motivoTransmissaoId) {
		this.motivoTransmissaoId = motivoTransmissaoId;
	}

	public Float getNivelCombustivel() {
		return nivelCombustivel;
	}

	public void setNivelCombustivel(Float nivelCombustivel) {
		this.nivelCombustivel = nivelCombustivel;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public Integer getNumEntrada2() {
		return numEntrada2;
	}

	public void setNumEntrada2(Integer numEntrada2) {
		this.numEntrada2 = numEntrada2;
	}

	public Integer getNumEntrada3() {
		return numEntrada3;
	}

	public void setNumEntrada3(Integer numEntrada3) {
		this.numEntrada3 = numEntrada3;
	}

	public Integer getNumEntrada4() {
		return numEntrada4;
	}

	public void setNumEntrada4(Integer numEntrada4) {
		this.numEntrada4 = numEntrada4;
	}

	public String getNumeroTel() {
		return numeroTel;
	}

	public void setNumeroTel(String numeroTel) {
		this.numeroTel = numeroTel;
	}

	public Double getOdometro() {
		return odometro;
	}

	public void setOdometro(Double odometro) {
		this.odometro = odometro;
	}

	public String getPanico() {
		return panico;
	}

	public void setPanico(String panico) {
		this.panico = panico;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getPontoDescricao() {
		return pontoDescricao;
	}

	public void setPontoDescricao(String pontoDescricao) {
		this.pontoDescricao = pontoDescricao;
	}

	public Integer getPontoId() {
		return pontoId;
	}

	public void setPontoId(Integer pontoId) {
		this.pontoId = pontoId;
	}

	public Integer getQtdeSatelite() {
		return qtdeSatelite;
	}

	public void setQtdeSatelite(Integer qtdeSatelite) {
		this.qtdeSatelite = qtdeSatelite;
	}

	public Integer getRPM() {
		return RPM;
	}

	public void setRPM(Integer rPM) {
		RPM = rPM;
	}

	public String getSerialChip() {
		return serialChip;
	}

	public void setSerialChip(String serialChip) {
		this.serialChip = serialChip;
	}

	public Boolean getStaEntrada1() {
		return staEntrada1;
	}

	public void setStaEntrada1(Boolean staEntrada1) {
		this.staEntrada1 = staEntrada1;
	}

	public Boolean getStaEntrada2() {
		return staEntrada2;
	}

	public void setStaEntrada2(Boolean staEntrada2) {
		this.staEntrada2 = staEntrada2;
	}

	public Boolean getStaEntrada3() {
		return staEntrada3;
	}

	public void setStaEntrada3(Boolean staEntrada3) {
		this.staEntrada3 = staEntrada3;
	}

	public Boolean getStaEntrada4() {
		return staEntrada4;
	}

	public void setStaEntrada4(Boolean staEntrada4) {
		this.staEntrada4 = staEntrada4;
	}

	public Boolean getStaEntrada5() {
		return staEntrada5;
	}

	public void setStaEntrada5(Boolean staEntrada5) {
		this.staEntrada5 = staEntrada5;
	}

	public Boolean getStaEntrada6() {
		return staEntrada6;
	}

	public void setStaEntrada6(Boolean staEntrada6) {
		this.staEntrada6 = staEntrada6;
	}

	public Boolean getStaEntrada7() {
		return staEntrada7;
	}

	public void setStaEntrada7(Boolean staEntrada7) {
		this.staEntrada7 = staEntrada7;
	}

	public Boolean getStaEntrada8() {
		return staEntrada8;
	}

	public void setStaEntrada8(Boolean staEntrada8) {
		this.staEntrada8 = staEntrada8;
	}

	public Boolean getStaSaida1() {
		return staSaida1;
	}

	public void setStaSaida1(Boolean staSaida1) {
		this.staSaida1 = staSaida1;
	}

	public Boolean getStaSaida2() {
		return staSaida2;
	}

	public void setStaSaida2(Boolean staSaida2) {
		this.staSaida2 = staSaida2;
	}

	public Boolean getStaSaida3() {
		return staSaida3;
	}

	public void setStaSaida3(Boolean staSaida3) {
		this.staSaida3 = staSaida3;
	}

	public Boolean getStaSaida4() {
		return staSaida4;
	}

	public void setStaSaida4(Boolean staSaida4) {
		this.staSaida4 = staSaida4;
	}

	public Boolean getStaSaida5() {
		return staSaida5;
	}

	public void setStaSaida5(Boolean staSaida5) {
		this.staSaida5 = staSaida5;
	}

	public Boolean getStaSaida6() {
		return staSaida6;
	}

	public void setStaSaida6(Boolean staSaida6) {
		this.staSaida6 = staSaida6;
	}

	public Boolean getStaSaida7() {
		return staSaida7;
	}

	public void setStaSaida7(Boolean staSaida7) {
		this.staSaida7 = staSaida7;
	}

	public Boolean getStaSaida8() {
		return staSaida8;
	}

	public void setStaSaida8(Boolean staSaida8) {
		this.staSaida8 = staSaida8;
	}

	public Double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}

	public Double getTensao() {
		return tensao;
	}

	public void setTensao(Double tensao) {
		this.tensao = tensao;
	}

	public Integer getTotalMensagemAberta() {
		return totalMensagemAberta;
	}

	public void setTotalMensagemAberta(Integer totalMensagemAberta) {
		this.totalMensagemAberta = totalMensagemAberta;
	}

	public Integer getTotalOcorrenciaAberta() {
		return totalOcorrenciaAberta;
	}

	public void setTotalOcorrenciaAberta(Integer totalOcorrenciaAberta) {
		this.totalOcorrenciaAberta = totalOcorrenciaAberta;
	}

	public Integer getTotalRotaAtiva() {
		return totalRotaAtiva;
	}

	public void setTotalRotaAtiva(Integer totalRotaAtiva) {
		this.totalRotaAtiva = totalRotaAtiva;
	}

	public Integer getVeiculoId() {
		return veiculoId;
	}

	public void setVeiculoId(Integer veiculoId) {
		this.veiculoId = veiculoId;
	}

	public Double getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(Double velocidade) {
		this.velocidade = velocidade;
	}
	/*
	 * converte array de json em objetos e os salva
	 */
	public static void saveFromJson(JsonArray jsonArray) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Gson gson = new Gson();
		jsonArray.forEach(json -> session.save(gson.fromJson(json, Posicao.class)));
		tx.commit();
		session.close();
	}
}
