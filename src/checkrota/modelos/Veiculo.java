package checkrota.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import checkrota.bancodedados.HibernateUtil;

@Entity
@Table( name = "veiculos" )
public class Veiculo {
	
	private Long id;
	private String descricao;
	@Column(unique = true)
	private String placa;
	
	@Id
	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public void setId(Long id) {
		this.id = id;
	}
	/*
	 * converte array de json em objetos e os salva
	 */
	public static void saveFromJson(JsonArray jsonArray) {
		if(jsonArray == null)
			return;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Gson gson = new Gson();
		jsonArray.forEach(json -> session.save(gson.fromJson(json, Veiculo.class)));
		tx.commit();
		session.close();
	}
}
