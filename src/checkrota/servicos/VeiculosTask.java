package checkrota.servicos;

import java.io.IOException;
import java.util.TimerTask;

import checkrota.modelos.Veiculo;

public class VeiculosTask extends TimerTask {
	private ApiCheckrota api;	
	
	public VeiculosTask(ApiCheckrota api) {
		super();
		// referencia da class ApiCheckrota
		this.api = api;
	}
	
	/*
	 * Executa rotina de atualização de veiculos
	 */
	@Override
	public void run() {
		try {
			Veiculo.saveFromJson(api.veiculos());
		} catch (IOException e) { System.out.println(e.getMessage()); }
	}

}
