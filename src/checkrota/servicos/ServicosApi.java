package checkrota.servicos;

import java.io.IOException;
import java.util.Map;
import java.util.Timer;

import checkrota.util.Propriedades;

public class ServicosApi implements Runnable {
	
	private ApiCheckrota api;
	private Map<String, String> props;
	private long update_posicoes;
	private long update_veiculos;
	private long update_categorias;
	
	/*
	 * Inicialização de ApiCheckrota
	 * carregamento de propriedades do arquivo 'configuracoes.properties'
	 *  e inicialização de tempos padrões de carregamento de dados
	 */
	public ServicosApi() {
		api = new ApiCheckrota();
		props = Propriedades.valores();
		
		System.out.println("Configurações:");
		props.forEach((key,value) -> System.out.println(key+" = "+value));
		
		update_posicoes = Integer.parseInt(props.getOrDefault("interval_posicoes", "1"))*60*1000;
		update_categorias = Integer.parseInt(props.getOrDefault("interval_categorias", "5"))*60*1000;
		update_veiculos = Integer.parseInt(props.getOrDefault("interval_veiculos", "5"))*60*1000;
	}
	
	
	/*
	 * Ao executar este runnable serão atualizadas todas as informações
	 * como Categorias,Veiculos, e posições
	 * 
	 * cada uma destas atualizações obdessem aos tempos configurados
	 * 
	 */
	
	@Override
	public void run() {
		
		try {
			// autenticação no servidor da checkrota
			api.autenticacao(props.get("username"),props.get("password"));
			Thread.sleep(1000);
			
			/*
			 * Serviços a serem executados
			 * ao iniciar a aplicação eles são atualizados na primeira vez, e depois em ciclos 
			 */
			new Timer().schedule(new CategoriasTask(api), 5000 , update_categorias);
			new Timer().schedule(new VeiculosTask(api), 5000 , update_veiculos);
			new Timer().schedule(new PosicoesTask(api), 10000 , update_posicoes);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (InterruptedException e) { 
			System.out.println(e.getMessage()); 
		}

	}
}