package checkrota.servicos;

import java.io.IOException;
import java.util.Map;

import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import checkrota.util.Propriedades;

public class ApiCheckrota {
	private String token;
	// propriedades carregadas de configuracoes.properties
	private Map<String, String> props = Propriedades.valores();
	
	public boolean isTokenDisponivel() {
		return token != null;
	}
	
	/*
	 * Método recebe via post os dados de autenticação, e retorna uma string (token), 
	 * que deve ser enviado em todas as chamados dos método no header Authorization
	 */
	public void autenticacao(String username, String password) throws IOException {
		Response response = Jsoup.connect(props.get("url_servico")+"autenticacao").method(Method.POST).data("username", username).ignoreContentType(true).data("password",password).execute();
		System.out.println((token = response.body().trim()));
	}
	
	/*
	 * 
	 * Recebe o token header Authorization, e retorna a lista de veiculos
	 * 
	 */
	public JsonArray veiculos() throws IOException {
		Response response = Jsoup.connect(props.get("url_servico")+"veiculo/lista").method(Method.GET).ignoreContentType(true).header("Authorization", token).execute();

		return new JsonParser()
				.parse(response.body())
					.getAsJsonObject().get("veiculos")
						.getAsJsonObject().get("veiculo").getAsJsonArray();
	}
	
	/*
	 * 
	 * Recebe o token header Authorization, e retorna a lista de categoria do veículo
	 * 
	 */
	public JsonArray categoriasVeiculos() throws IOException {
		Response response = Jsoup.connect(props.get("url_servico")+"categoriaVeiculo/lista").method(Method.GET).ignoreContentType(true).header("Authorization", token).execute();
		JsonObject resultado = new JsonParser().parse(response.body()).getAsJsonObject();
		if (resultado.get("categoriaVeiculos").isJsonArray()) {
			return resultado.get("categoriaVeiculos").getAsJsonArray();
		} else {
			return null;
		}
	}
	
	/*
	 * 
	 * Método usado para recuperar a ultima posição do(s) veículo(s)
	 * 
	 */
	public JsonArray ultimaPosicao(String veiculoId,String categoriaId) throws IOException {
		Response response = Jsoup.connect(props.get("url_servico")+"monitoramento/monitoramentoVeiculoGrid/0/"+veiculoId+"/"+categoriaId).method(Method.GET).ignoreContentType(true).header("Authorization", token).execute();
		
		return new JsonParser()
				.parse(response.body())
					.getAsJsonObject().get("monitoramentoVeiculos")
						.getAsJsonObject().get("monitoramentoVeiculo").getAsJsonArray();
	}
	
}
