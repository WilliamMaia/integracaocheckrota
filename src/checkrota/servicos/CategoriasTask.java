package checkrota.servicos;

import java.io.IOException;
import java.util.TimerTask;

import checkrota.modelos.Categoria;

public class CategoriasTask extends TimerTask {
	private ApiCheckrota api;	
	
	public CategoriasTask(ApiCheckrota api) {
		super();
		// referencia da class ApiCheckrota
		this.api = api;
	}
	
	/*
	 * Executa rotina de atualização de categorias
	 */
	@Override
	public void run() {
		try {
			Categoria.saveFromJson(api.categoriasVeiculos());
		} catch (IOException e) { System.out.println(e.getMessage()); }
	}

}
