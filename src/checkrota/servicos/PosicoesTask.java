package checkrota.servicos;

import java.io.IOException;
import java.util.TimerTask;

import checkrota.modelos.Posicao;

public class PosicoesTask extends TimerTask {
	private ApiCheckrota api;	
	
	public PosicoesTask(ApiCheckrota api) {
		super();
		// referencia da class ApiCheckrota
		this.api = api;
	}
	/*
	 * Executa rotina de atualização de posicoes
	 */
	@Override
	public void run() {
		try {
			Posicao.saveFromJson(api.ultimaPosicao("0", "0"));
		} catch (IOException e) { System.out.println(e.getMessage()); }
	}

}
