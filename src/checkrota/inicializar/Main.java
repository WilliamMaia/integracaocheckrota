package checkrota.inicializar;

import checkrota.bancodedados.HibernateUtil;
import checkrota.servicos.ServicosApi;

public class Main {
	public static void main(String[] args) {
		/*
		 * Abertura de conexão com hibernate e inicio a classe de serviços
		 */
		HibernateUtil.getSessionFactory().openSession();
		new Thread(new ServicosApi()).start();
	}	
}

